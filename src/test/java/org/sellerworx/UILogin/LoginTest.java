package org.sellerworx.UILogin;

import org.sellerworx.api.get.persistent.TestData_API_V5Persistent;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sellerworx.api.utils.ListAssert;
import com.sellerworx.api.utils.PropertiesFile;

import page.all.common.LoginPage;
import page.test.common.BaseTestObject;

public class LoginTest extends BaseTestObject{
	
	LoginPage login = null;
	ListAssert Assert = new ListAssert();
	TestData_API_V5Persistent testData = new TestData_API_V5Persistent();

	// Variables
	String correctemail = PropertiesFile.getloginPropertiesData("correctEmail");
	String correctpassword = PropertiesFile
			.getloginPropertiesData("correctPassword");
	String Code = null;
	
	@Test(enabled = true)
	public void ValidLoginTest()throws Exception{
		try{
			Reporter.log("Started test case : LoginValidTest ", true);
			login = new LoginPage(webdriver);
			login.enterEmail(correctemail);
			login.enterPassword(correctpassword);
			login.clickSignInBtn(true);
			testData.setCode(login.redirectOauthURL(true));
			
			
			Reporter.log("Completed test case : LoginValidTest ", true);
			
		}catch(Exception e){
			Reporter.log("Failed test case : LoginValidTest", true);
			throw new Exception(e);
		}
	}

}
