package org.sellerworx.api.get.persistent;

public class TestData_API_V5Persistent {

	private String code = null;
	private String AccessToken = null;
	private String RefreshToken = null;
	
	public String getCode(){
		return code;
	}
	
	public void setCode(String code){
		this.code = code;
	}
	
	public void setAccessToken(String AccessToken){
		this.AccessToken = AccessToken;
	}
	
	public String getAccessToken(){
		return AccessToken;
	}
	
	public void setRefreshToken(String RefreshToken){
		this.RefreshToken = RefreshToken;
	}
	
	public String getRefreshToken(){
		return RefreshToken;
	}
}
