package org.sellerworx.api.v5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sellerworx.api.get.persistent.TestData_API_V5Persistent;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sellerworx.api.utils.JsonConvertor;
import com.sellerworx.api.utils.ListAssert;
import com.sellerworx.api.utils.PropertiesFile;

public class OrdersTest {
	
	//Variables
	private String bearerAccessToken = null;
	private String endpoint = PropertiesFile.getresourcePropertiesData("endpoint");
	private List<String> lst = null;
	private List<String> lstKeys = new ArrayList<String>();
	private List<String> lstKeys1 = new ArrayList<String>();
	private List<String> lstKeys2 = new ArrayList<String>();
	private List<String> lstKeys3 = new ArrayList<String>();
	private List<String> lstKeys4 = new ArrayList<String>();
	private String[] arrStatus = { "PICK_LIST_GEN", "PENDING_INVOICE",
			"READY_TO_SHIP", "MANIFESTED", "SHIPPED", "COMPLETED" };
	
	
	@SuppressWarnings("serial")
	List<String> listOrder1 = new ArrayList<String>(){
		{
			add("address");
			add("address_id");
			add("can_fulfill");
			add("cancelled_on");
			add("channel_order_id");
			add("channel_orderitem_id");
			add("channel_sku");
			add("channel_status");
			add("created_at");
			add("customer");
			add("customer_id");
			add("id");
			add("image");
			add("is_label_printed");
			add("manifest_id");
			add("mrp");
			add("ordered_on");
			add("price");
			add("quantity");
			add("returned_on");
			add("ship_after");
			add("ship_by");
			add("shipment");
			add("shipping_charge");
			add("sku");
			add("sku_channel_id");
			add("status");
			add("title");
			add("total_price");
			add("type_of_order");
			add("updated_at");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstOrderId = new ArrayList<String>(){
		{
			add("address");
			add("address_id");
			add("can_fulfill");
			add("cancelled_on");
			add("channel_order_id");
			add("channel_orderitem_id");
			add("channel_sku");
			add("channel_status");
			add("created_at");
			add("customer");
			add("customer_id");
			add("id");
			add("image");
			add("invoice");
			add("is_label_printed");
			add("manifest_id");
			add("mrp");
			add("ordered_on");
			add("price");
			add("quantity");
			add("returned_on");
			add("ship_after");
			add("ship_by");
			add("shipment");
			add("shipping_charge");
			add("sku");
			add("sku_channel_id");
			add("status");
			add("title");
			add("total_price");
			add("type_of_order");
			add("updated_at");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstAddress = new ArrayList<String>(){
		{
			add("address_1");
			add("address_2");
			add("address_3");
			add("city");
			add("country");
			add("country_code");
			add("country_id");
			add("id");
			add("name");
			add("pincode");
			add("shipping_address");
			add("state");
			add("state_id");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstCustomer = new ArrayList<String>(){
		{
			add("address_1");
			add("address_2");
			add("address_3");
			add("city");
			add("country_id");
			add("created_at");
			add("email");
			add("id");
			add("name");
			add("phone");
			add("pincode");
			add("ship_to_name");
			add("shipping_address");
			add("state_id");
			add("updated_at");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstChannelSku = new ArrayList<String>(){
		{
			add("active");
			add("avg_orders_count_per_day");
			add("channel");
			add("channel_id");
			add("channel_stock");
			add("competitive_price");
			add("created_at");
			add("currency_code");
			add("id");
			add("image");
			add("is_fbm");
			add("last_synced");
			add("last_upsynced");
			add("local_shipping");
			add("marketplace_id");
			add("mrp");
			add("national_shipping");
			add("original_sku_id");
			add("parent_sku");
			add("price");
			add("sale_end_date");
			add("sale_start_date");
			add("sku");
			add("sku_id");
			add("stock");
			add("title");
			add("updated_at");
			add("zonal_shipping");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstChannel = new ArrayList<String>(){
		{
			add("active");
			add("connected");
			add("description");
			add("id");
			add("master_channel");
			add("name");
			add("seller_id");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstParentSku = new ArrayList<String>(){
		{
			add("active");
			add("avg_orders_count_per_day");
			add("created_at");
			add("deleted_at");
			add("has_mismatch");
			add("id");
			add("image");
			add("original_sku");
			add("sku");
			add("stock");
			add("title");
			add("update_disabled");
			add("updated_at");
			add("weight");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstMasterChannel = new ArrayList<String>(){
		{
			add("code");
			add("description");
			add("id");
			add("name");
			add("visible");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstInvoice = new ArrayList<String>(){
		{
			add("amount");
			add("created_at");
			add("date");
			add("id");
			add("imei");
			add("number");
			add("seller_id");
			add("tax_amount");
			add("updated_at");
		}
	};
	
	@SuppressWarnings("serial")
	List<String> lstError = new ArrayList<String>(){
		{
			add("channel_orderitem_id");
			add("error");
			add("id");
			add("order_status");
			add("type");	
		}
	};
	
	private JSONObject jsonObjectResponse = null;
	private JSONArray jsonArrayResponse = null;
	private JSONObject jsonObject = null;
	private JSONObject jsonObject2 = null;
	private JSONObject jsonObject3 = null;
	private JSONObject jsonObject4 = null;

	
	private ListAssert listAssert = new ListAssert();
	JsonConvertor jsonConvertor = new JsonConvertor();
	TestData_API_V5Persistent testData = new TestData_API_V5Persistent();
	static Logger logInstruction = LogManager.getLogger(OrdersTest.class.getName());
	
	/**
	 * This test will validate the GET api :- route+api/v5/orders
	 * @throws Exception 
	 */
	@Test(enabled =  true, priority = 0)
	public void AllOrdersGetTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		try{
			logInstruction.debug("Test case started = AllOrdersGetTest");
			logInstruction.debug("Requesting URL : " + endpoint + "api/v5/orders");
			url = new URL(endpoint + "api/v5/orders");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = jsonObjectResponse.getJSONArray("items");
				for(int i = 0; i < jsonArrayResponse.length(); i++ ){
					lstKeys = new ArrayList<String>();
					jsonObject = (JSONObject) jsonArrayResponse.get(i);
					for(int var1 = 0 ; var1 < jsonObject.names().length(); var1++){
						lstKeys.add(jsonObject.names().getString(var1));	
					}
					Collections.sort(lstKeys);
					listAssert.assertTrue(listOrder1.containsAll(lstKeys), 
							"\nKEYS ARE MISSING IN ORDERS" );
					listAssert.assertTrue(listOrder1.size() == lstKeys.size(), 
							"\nKEYS COUNT IS DIFFERENT IN ORDERS");
					if(lstKeys.get(0).equals("address")){
						lstKeys1 = new ArrayList<String>();
						jsonObject2 = (JSONObject) jsonObject.get("address");
						for(int var2 = 0; var2 < jsonObject2.length(); var2++){
							lstKeys1.add(jsonObject2.names().getString(var2));
						}
						Collections.sort(lstKeys1);
						listAssert.assertTrue(lstAddress.containsAll(lstKeys1), 
								"\nKEYS ARE MISSING IN ADDRESS");
						listAssert.assertTrue(lstAddress.size() == lstKeys1.size(), 
								"\nKEYS COUNT IS DIFFERENT IN ADDRESS");
					}
					if(lstKeys.get(9).equals("customer")){
						lstKeys2 = new ArrayList<String>();
						jsonObject2 = (JSONObject) jsonObject.get("customer");
						for(int var2 = 0; var2 < jsonObject2.length(); var2++){
							lstKeys2.add(jsonObject2.names().getString(var2));
						}
						Collections.sort(lstKeys2);
						listAssert.assertTrue(lstCustomer.containsAll(lstKeys2), 
								"\nKEYS ARE MISSING IN CUSTOMER");
						listAssert.assertTrue(lstCustomer.size() == lstKeys2.size(), 
								"\nKEYS COUNT IS DIFFERENT IN CUSTOMER");
					}
					if(lstKeys.get(6).equals("channel_sku")){
						lstKeys2 = new ArrayList<String>();
						jsonObject2 = (JSONObject) jsonObject.get("channel_sku");
						for(int var2 = 0; var2 < jsonObject2.length(); var2++){
							lstKeys2.add(jsonObject2.names().getString(var2));
						}
						Collections.sort(lstKeys2);
						listAssert.assertTrue(lstChannelSku.containsAll(lstKeys2), 
								"\nKEYS ARE MISSING IN CHANNEL SKU");
						listAssert.assertTrue(lstChannelSku.size() == lstKeys2.size(), 
								"\nKEYS COUNT IS DIFFERENT IN CHANNEL SKU");
						if(lstKeys2.get(2).equals("channel")){
							lstKeys3 = new ArrayList<String>();
							jsonObject3 = (JSONObject) jsonObject2.get("channel");
							for(int var3 = 0; var3 < jsonObject3.length(); var3++){
								lstKeys3.add(jsonObject3.names().getString(var3));
							}
							Collections.sort(lstKeys3);
							listAssert.assertTrue(lstChannel.containsAll(lstKeys3), 
									"\nKEYS ARE MISSING IN CHANNEL");
							listAssert.assertTrue(lstChannel.size() == lstKeys3.size(), 
									"\nKEYS COUNT IS DIFFERENT IN CHANNEL");			
							if(lstKeys3.get(4).equals("master_channel")){
								lstKeys4 = new ArrayList<String>();
								jsonObject4 = (JSONObject) jsonObject3.get("master_channel");
								for(int var4 = 0; var4 < jsonObject4.length(); var4++){
									lstKeys4.add(jsonObject4.names().getString(var4));
								}
								Collections.sort(lstKeys4);
								listAssert.assertTrue(lstMasterChannel.containsAll(lstKeys4), 
										"\nKEYS ARE MISSING IN MASTER CHANNEL");
								listAssert.assertTrue(lstMasterChannel.size() == lstKeys4.size(), 
										"\nKEYS COUNT IS DIFFERENT IN MASTER CHANNEL");	
							}	
						}
						
						if(lstKeys2.get(18).equals("parent_sku")){
							lstKeys3 = new ArrayList<String>();
							jsonObject3 = (JSONObject) jsonObject2.get("parent_sku");
							for(int var3 = 0; var3 < jsonObject3.length(); var3++){
								lstKeys3.add(jsonObject3.names().getString(var3));
							}
							Collections.sort(lstKeys3);
							listAssert.assertTrue(lstParentSku.containsAll(lstKeys3), 
									"\nKEYS ARE MISSING IN PARENT SKU");
							listAssert.assertTrue(lstParentSku.size() == lstKeys3.size(), 
									"\nKEYS COUNT IS DIFFERENT IN PARENT SKU");
						}
					}
				}
			}
			logInstruction.debug("Test case completed = AllOrdersGetTest");
			listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = AllOrdersGetTest");
			e.printStackTrace();
		}
		
	}

	
	/**
	 * This test will validate the GET api :- route+api/v5/orders/<id>
	 * @throws Exception 
	 */
	@Test(enabled =  true, priority = 1)
	public void OneOrdersGetTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		try{
			logInstruction.debug("Test case started = OneOrdersGetTest");
			url = new URL(endpoint + "api/v5/orders");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				for (int i = 0; i < jsonArrayResponse.length(); i++) {
					jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
					lst.add(jsonObject2.get("id").toString());
				}
				Collections.sort(lst);
				for(int var1 = 0; var1 < lst.size(); var1++){
					logInstruction.debug("Requesting URL = " + endpoint+"api/v5/orders/"+lst.get(var1));
					url = new URL(endpoint + "api/v5/orders/" + lst.get(var1));
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setDoOutput(true);
					httpURLConnection.setRequestMethod("GET");
					if(bearerAccessToken != null && bearerAccessToken.length() > 0){
						httpURLConnection.
							setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
					}
					if(httpURLConnection.getResponseCode() == 200){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
						listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
						bufferedReader = new BufferedReader(new InputStreamReader(
								httpURLConnection.getInputStream()));
						jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);lstKeys = new ArrayList<String>();
							for (int i = 0; i < jsonObjectResponse.names().length(); i++) {
								lstKeys.add(jsonObjectResponse.names().getString(i));
							}
						Collections.sort(lstKeys);
						listAssert.assertTrue(lstOrderId.
								containsAll(lstKeys),"KEYS ARE MISSING IN ORDER");
						listAssert.assertTrue(
								lstOrderId.size() == lstKeys.size(),
								"KEYS COUNT IS DIFFERENT IN ORDER");
						if(lstKeys.get(0).equals("address")){
							lstKeys1 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObjectResponse.get("address");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys1.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys1);
							listAssert.assertTrue(lstAddress.containsAll(lstKeys1), 
									"\nKEYS ARE MISSING IN ADDRESS");
							listAssert.assertTrue(lstAddress.size() == lstKeys1.size(), 
									"\nKEYS COUNT IS DIFFERENT IN ADDRESS");
						}
						if(lstKeys.get(9).equals("customer")){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObjectResponse.get("customer");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys2.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstCustomer.containsAll(lstKeys2), 
									"\nKEYS ARE MISSING IN CUSTOMER");
							listAssert.assertTrue(lstCustomer.size() == lstKeys2.size(), 
									"\nKEYS COUNT IS DIFFERENT IN CUSTOMER");
						}
						if(lstKeys.get(6).equals("channel_sku")){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObjectResponse.get("channel_sku");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys2.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstChannelSku.containsAll(lstKeys2), 
									"\nKEYS ARE MISSING IN CHANNEL SKU");
							listAssert.assertTrue(lstChannelSku.size() == lstKeys2.size(), 
									"\nKEYS COUNT IS DIFFERENT IN CHANNEL SKU");
							if(lstKeys2.get(2).equals("channel")){
								lstKeys3 = new ArrayList<String>();
								jsonObject3 = (JSONObject) jsonObject2.get("channel");
								for(int var3 = 0; var3 < jsonObject3.length(); var3++){
									lstKeys3.add(jsonObject3.names().getString(var3));
								}
								Collections.sort(lstKeys3);
								listAssert.assertTrue(lstChannel.containsAll(lstKeys3), 
										"\nKEYS ARE MISSING IN CHANNEL");
								listAssert.assertTrue(lstChannel.size() == lstKeys3.size(), 
										"\nKEYS COUNT IS DIFFERENT IN CHANNEL");			
								if(lstKeys3.get(4).equals("master_channel")){
									lstKeys4 = new ArrayList<String>();
									jsonObject4 = (JSONObject) jsonObject3.get("master_channel");
									for(int var4 = 0; var4 < jsonObject4.length(); var4++){
										lstKeys4.add(jsonObject4.names().getString(var4));
									}
									Collections.sort(lstKeys4);
									listAssert.assertTrue(lstMasterChannel.containsAll(lstKeys4), 
											"\nKEYS ARE MISSING IN MASTER CHANNEL");
									listAssert.assertTrue(lstMasterChannel.size() == lstKeys4.size(), 
											"\nKEYS COUNT IS DIFFERENT IN MASTER CHANNEL");	
								}	
							}
							if(lstKeys2.get(18).equals("parent_sku")){
								lstKeys3 = new ArrayList<String>();
								jsonObject3 = (JSONObject) jsonObject2.get("parent_sku");
								for(int var3 = 0; var3 < jsonObject3.length(); var3++){
									lstKeys3.add(jsonObject3.names().getString(var3));
								}
								Collections.sort(lstKeys3);
								listAssert.assertTrue(lstParentSku.containsAll(lstKeys3), 
										"\nKEYS ARE MISSING IN PARENT SKU");
								listAssert.assertTrue(lstParentSku.size() == lstKeys3.size(), 
										"\nKEYS COUNT IS DIFFERENT IN PARENT SKU");
							}
						}
						if(lstKeys.get(13).equals("invoice")){
							lstKeys3 = new ArrayList<String>();
							if(!jsonObjectResponse.get("invoice").equals(null)){
								jsonObject4 = (JSONObject) jsonObjectResponse.get("invoice");
								for(int var3 = 0; var3 < jsonObject4.length(); var3++){
									lstKeys3.add(jsonObject4.names().getString(var3));
								}
								Collections.sort(lstKeys3);
								listAssert.assertTrue(lstInvoice.containsAll(lstKeys3), 
										"\nKEYS ARE MISSING IN INVOICE");
								listAssert.assertTrue(lstInvoice.size() == lstKeys3.size(), 
										"\nKEYS COUNT IS DIFFERENT IN INVOICE");
							}
						}
					}
				}
			}
			logInstruction.debug("Test case completed = OneOrdersGetTest");
			listAssert.assertAll();	
			}catch(Exception e){
				logInstruction.fatal("Test case got failed = OneOrdersGetTest");
				e.printStackTrace();
		}
	}

	/**
	 * This test will validate the GET api :- route+api/v5/orders?filters={"status":<status>}
	 * @throws Exception 
	 */
	@Test(enabled =  true, priority = 2)
	public void OneStatusOrdersGetTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		
		try{
			logInstruction.debug("Test case started = OneStatusOrdersGetTest");
			for(int arr = 0; arr < arrStatus.length; arr++){
				logInstruction.debug("Requesting URL : " + endpoint
						+ "api/v5/orders?filters={\"status\":\"" 
						+ arrStatus[arr] + "\"}");
				url = new URL(endpoint + "api/v5/orders?filters={\"status\":\""
						+ arrStatus[arr] + "\"}");
				httpURLConnection = (HttpURLConnection) url.openConnection();
				httpURLConnection.setDoOutput(true);
				httpURLConnection.setRequestMethod("GET");
				if(bearerAccessToken != null && bearerAccessToken.length() > 0){
					httpURLConnection.
						setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
				}
				if(httpURLConnection.getResponseCode() == 200){
					listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
					bufferedReader = new BufferedReader(new InputStreamReader(
							httpURLConnection.getInputStream()));
					jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
					jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
					if(jsonArrayResponse.length() > 0){
						for(int i = 0; i < jsonArrayResponse.length(); i++ ){
							lstKeys = new ArrayList<String>();
							jsonObject = (JSONObject) jsonArrayResponse.get(i);
							for(int var1 = 0 ; var1 < jsonObject.names().length(); var1++){
								lstKeys.add(jsonObject.names().getString(var1));	
							}
						Collections.sort(lstKeys);
						listAssert.assertTrue(listOrder1.containsAll(lstKeys), 
								"\nKEYS ARE MISSING IN ORDERS" );
						listAssert.assertTrue(listOrder1.size() == lstKeys.size(), 
								"\nKEYS COUNT IS DIFFERENT IN ORDERS");
						if(lstKeys.get(0).equals("address")){
							lstKeys1 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObject.get("address");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys1.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys1);
							listAssert.assertTrue(lstAddress.containsAll(lstKeys1), 
									"\nKEYS ARE MISSING IN ADDRESS");
							listAssert.assertTrue(lstAddress.size() == lstKeys1.size(), 
									"\nKEYS COUNT IS DIFFERENT IN ADDRESS");
						}
						if(lstKeys.get(9).equals("customer")){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObject.get("customer");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys2.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstCustomer.containsAll(lstKeys2), 
									"\nKEYS ARE MISSING IN CUSTOMER");
							listAssert.assertTrue(lstCustomer.size() == lstKeys2.size(), 
									"\nKEYS COUNT IS DIFFERENT IN CUSTOMER");
						}
						if(lstKeys.get(6).equals("channel_sku")){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = (JSONObject) jsonObject.get("channel_sku");
							for(int var2 = 0; var2 < jsonObject2.length(); var2++){
								lstKeys2.add(jsonObject2.names().getString(var2));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstChannelSku.containsAll(lstKeys2), 
									"\nKEYS ARE MISSING IN CHANNEL SKU");
							listAssert.assertTrue(lstChannelSku.size() == lstKeys2.size(), 
									"\nKEYS COUNT IS DIFFERENT IN CHANNEL SKU");
							if(lstKeys2.get(2).equals("channel")){
								lstKeys3 = new ArrayList<String>();
								jsonObject3 = (JSONObject) jsonObject2.get("channel");
								for(int var3 = 0; var3 < jsonObject3.length(); var3++){
									lstKeys3.add(jsonObject3.names().getString(var3));
								}
								Collections.sort(lstKeys3);
								listAssert.assertTrue(lstChannel.containsAll(lstKeys3), 
										"\nKEYS ARE MISSING IN CHANNEL");
								listAssert.assertTrue(lstChannel.size() == lstKeys3.size(), 
										"\nKEYS COUNT IS DIFFERENT IN CHANNEL");			
								if(lstKeys3.get(4).equals("master_channel")){
									lstKeys4 = new ArrayList<String>();
									jsonObject4 = (JSONObject) jsonObject3.get("master_channel");
									for(int var4 = 0; var4 < jsonObject4.length(); var4++){
										lstKeys4.add(jsonObject4.names().getString(var4));
									}
									Collections.sort(lstKeys4);
									listAssert.assertTrue(lstMasterChannel.containsAll(lstKeys4), 
											"\nKEYS ARE MISSING IN MASTER CHANNEL");
									listAssert.assertTrue(lstMasterChannel.size() == lstKeys4.size(), 
											"\nKEYS COUNT IS DIFFERENT IN MASTER CHANNEL");	
								}	
							}
						}
						}
					}
				}
			}
			logInstruction.debug("Test case completed = OneStatusOrdersGetTest");
			listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = OneStatusOrdersGetTest");
			e.printStackTrace();
		}
	}

	/**
	 * This test will validate the PUT api :- route+api/v5/orders/<id>
	 * @throws Exception 
	 */
	@Test(enabled = true, priority = 3)
	public void OneOrderPutTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		String body = null;
		OutputStream os = null;

		LocalDateTime now = LocalDateTime.now();
		String date = now.getYear() + "-" + now.getMonthValue() + "-" + now.getDayOfMonth() + "00:00:00";
		
		try{
			logInstruction.debug("Test case started = OneOrderPutTest");
			url = new URL(endpoint + "api/v5/orders");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				for (int i = 0; i < jsonArrayResponse.length(); i++) {
					jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
					lst.add(jsonObject2.get("id").toString());
				}
				Collections.sort(lst);
				for(int var1 = 0; var1 < lst.size(); var1++){
					logInstruction.debug("Requesting URL = " + endpoint+"api/v5/orders/"+lst.get(var1));
					url = new URL(endpoint + "api/v5/orders/" + lst.get(var1));
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setDoOutput(true);
					String invNumber = "FK0000" + (var1 + 1);
					String Amount = "46" + (var1 + 1);
					body = "{ \"status\" : \"" + arrStatus[2] 
							+ "\",\"invoice\"    : {"
							+ "\"number\"      : \"" + invNumber 
							+ "\",\"amount\"     :   " + Amount
							+ ",\"date\"       : \"" + date
							+ "\",\"tax_amount\" : 0 } }";
					httpURLConnection.setRequestMethod("PUT");
					httpURLConnection.setRequestProperty("Content-Type", "application/json");
					
					if(bearerAccessToken != null && bearerAccessToken.length() > 0){
						httpURLConnection.
							setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
					}
					
                    
                    os = httpURLConnection.getOutputStream();
                    os.write(body.getBytes());
                    os.flush();
                    os.close();
                    
					if(httpURLConnection.getResponseCode() == 200){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
						listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
						bufferedReader = new BufferedReader(new InputStreamReader(
								httpURLConnection.getInputStream()));
						jsonObject2 = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
						listAssert.assertEquals(jsonObject2.names().getString(0), "status",
     							"STATUS KEY IS NOT PRESENT");
     					listAssert.assertEquals(jsonObject2.getString("status"),
     							"SUCCESS", "ORDER IS NOT UPDATED");
					}
					else if(httpURLConnection.getResponseCode() == 400){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
	                    		"STATUS CODE IS NOT 400") ;
	  					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
	  							"STATUS LINE IS NOT BAD REQUEST");
	  					bufferedReader = new BufferedReader(new InputStreamReader(
	 							httpURLConnection.getErrorStream()));
	  					jsonObject3 = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
	  					lstKeys = new ArrayList<String>();
	  					for(int var5 = 0; var5 < jsonObject3.length(); var5++){
 							lstKeys.add(jsonObject3.names().getString(var5));
 						} 
	  					Collections.sort(lstKeys);
	  					listAssert.assertTrue(lstError.
 								containsAll(lstKeys),"KEYS ARE MISSING");
 						listAssert.assertTrue(
 								lstError.size() == lstKeys.size(),
 								"KEYS COUNT IS DIFFERENT");
 						if(lstKeys.get(1).equals("error")){
 							jsonObject4 = jsonObject3.getJSONObject("error");
 							listAssert.assertEquals(jsonObject4.names().getString(0), "message",
 	     							"STATUS KEY IS NOT PRESENT");
 	     					listAssert.assertEquals(jsonObject4.getString("message"),
 	     							"Can not update the order status to READY_TO_SHIP", 
 	     							"MANIFESTED ORDER IS UPDATED RTS ");
 						}
					}
				}
			}
			logInstruction.debug("Test case completed = OneOrderPutTest");
			listAssert.assertAll();	
			}catch(Exception e){
				logInstruction.fatal("Test case got failed = OneOrderPutTest");
				e.printStackTrace();
		}
	}
	
}
