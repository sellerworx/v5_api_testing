package org.sellerworx.api.v5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sellerworx.api.get.persistent.TestData_API_V5Persistent;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sellerworx.api.utils.JsonConvertor;
import com.sellerworx.api.utils.ListAssert;
import com.sellerworx.api.utils.PropertiesFile;

public class SkuChannelTest {

	//Variables
	private String bearerAccessToken = null;
	private String endpoint = PropertiesFile.getresourcePropertiesData("endpoint");
	private List<String> lst = null;
	private List<String> lstKeys = new ArrayList<String>();
	private List<String> lstKeys4 = new ArrayList<String>();
	
	@SuppressWarnings("serial")
	private List<String> lstChannelSku = new ArrayList<String>(){
		{
			add("active");
			add("avg_orders_count_per_day");
			add("channel_id");
			add("channel_stock");
			add("competitive_price");
			add("created_at");
			add("currency_code");
			add("id");
			add("image");
			add("is_fbm");
			add("last_synced");
			add("last_upsynced");
			add("local_shipping");
			add("marketplace_id");
			add("mrp");
			add("national_shipping");
			add("original_sku_id");
			add("price");
			add("sale_end_date");
			add("sale_start_date");
			add("sku");
			add("sku_id");
			add("stock");
			add("title");
			add("updated_at");
			add("zonal_shipping");
		}
	};
	
 	
	@SuppressWarnings("serial")
	private List<String> lstChannelIdSku = new ArrayList<String>(){
		{
			add("active");
			add("avg_orders_count_per_day");
			add("channel");
			add("channel_id");
			add("channel_stock");
			add("competitive_price");
			add("created_at");
			add("currency_code");
			add("id");
			add("image");
			add("is_fbm");
			add("last_synced");
			add("last_upsynced");
			add("local_shipping");
			add("marketplace_id");
			add("mrp");
			add("national_shipping");
			add("original_sku_id");
			add("price");
			add("sale_end_date");
			add("sale_start_date");
			add("sku");
			add("sku_id");
			add("skus");
			add("stock");
			add("title");
			add("updated_at");
			add("zonal_shipping");
		}
	};

 	
	//Objects
	private JSONObject jsonObjectResponse = null;
	private JSONArray jsonArrayResponse = null;
	private JSONObject jsonObject2 = null;
	private JSONObject jsonObject3 = null;
	
	private JSONArray jsonArray = null;
	
	private ListAssert listAssert = new ListAssert();
	JsonConvertor jsonConvertor = new JsonConvertor();
	TestData_API_V5Persistent testData = new TestData_API_V5Persistent();
	static Logger logInstruction = LogManager.getLogger(SkuChannelTest.class.getName());
	
	
	/**
	 *This test will validate the GET api :- route+api/v5/skus/<id>/channel_skus
	 *@throws Exception 
	 * 
	*/
	@Test(enabled = true, priority = 0)
	public void AllChannelSkuGetTest()throws Exception{
		

		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		
		try{
			logInstruction.debug("Test case started = AllChannelSkuGetTest");
			url = new URL(endpoint + "api/v5/skus");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				for (int i = 0; i < jsonArrayResponse.length(); i++) {
					jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
					lst.add(jsonObject2.get("id").toString());
				}
				for(int var1 = 0; var1 < lst.size(); var1++){
					logInstruction.debug("Requesting URL = " + endpoint+"api/v5/skus/"+lst.get(var1)+"/channel_skus");
					
					url = new URL(endpoint + "api/v5/skus/" + lst.get(var1) + "/channel_skus");
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setDoOutput(true);
					httpURLConnection.setRequestMethod("GET");
					
					if(bearerAccessToken != null && bearerAccessToken.length() > 0){
						httpURLConnection.
							setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
					}
					
					if(httpURLConnection.getResponseCode() == 200){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
						listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
						bufferedReader = new BufferedReader(new InputStreamReader(
								httpURLConnection.getInputStream()));
						jsonArrayResponse = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
						
						for (int var4 = 0; var4 < jsonArrayResponse.length(); var4++) {
							jsonObject3=(JSONObject) jsonArrayResponse.get(var4);
							lstKeys = new ArrayList<String>();
							for (int i = 0; i < jsonObject3.names().length(); i++) {
								lstKeys.add(jsonObject3.names().getString(i));
							}
						Collections.sort(lstKeys);
						listAssert.assertTrue(lstChannelSku.
								containsAll(lstKeys),"KEYS ARE MISSING");
						listAssert.assertTrue(
								lstChannelSku.size() == lstKeys.size(),
								"KEYS COUNT IS DIFFERENT");
					}
					}
				}
			}
			logInstruction.debug("Test case completed = AllChannelSkuGetTest");
			listAssert.assertAll();
	}catch(Exception e){
		logInstruction.fatal("Test case got failed = AllChannelSkuGetTest");
		e.printStackTrace();
		}
	}

	
	/**
	 *This test will validate the GET api :- route+api/v5/skus/<id>/channel_skus/<id>
	 *@throws Exception 
	 * 
	*/
	@Test(enabled = true, priority = 1)
	public void OneSkuChannelGetTest()throws Exception{
		

		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		
		try{
			logInstruction.debug("Test case started = OneSkuChannelGetTest");
			url = new URL(endpoint + "api/v5/skus");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				for (int i = 0; i < jsonArrayResponse.length(); i++) {
					jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
					lst.add(jsonObject2.get("id").toString());
				}
				for(int var1 = 0; var1 < lst.size(); var1++){
					logInstruction.debug("Requesting URL = " + endpoint+"api/v5/skus/"+lst.get(var1)+"/channel_skus");
					
					url = new URL(endpoint + "api/v5/skus/" + lst.get(var1) + "/channel_skus");
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setDoOutput(true);
					httpURLConnection.setRequestMethod("GET");
					
					if(bearerAccessToken != null && bearerAccessToken.length() > 0){
						httpURLConnection.
							setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
					}
					
					if(httpURLConnection.getResponseCode() == 200){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
						listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
						bufferedReader = new BufferedReader(new InputStreamReader(
								httpURLConnection.getInputStream()));
						jsonArrayResponse = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
						
						for (int var4 = 0; var4 < jsonArrayResponse.length(); var4++) {
							jsonObject3=(JSONObject) jsonArrayResponse.get(var4);
							lstKeys = new ArrayList<String>();
							for (int i = 0; i < jsonObject3.names().length(); i++) {
								
								lstKeys.add(jsonObject3.names().getString(i));
							}
							for(int var5 = 0; var5 < lst.size(); var5++){
								logInstruction.debug("Requesting URL = " 
															+ endpoint + "api/v5/skus/" 
															+ lst.get(var5)+"/channel_skus/"
															+ jsonObject3.get("id"));
								url = new URL(endpoint + "api/v5/skus/" 
															+ lst.get(var5) 
															+ "/channel_skus/" 
															+ jsonObject3.get("id"));
								httpURLConnection = (HttpURLConnection) url.openConnection();
								httpURLConnection.setDoOutput(true);
								httpURLConnection.setRequestMethod("GET");
								if(bearerAccessToken != null && bearerAccessToken.length() > 0){
									httpURLConnection.
										setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
								}
								if(httpURLConnection.getResponseCode() == 200){
									listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
									listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
									bufferedReader = new BufferedReader(new InputStreamReader(
											httpURLConnection.getInputStream()));
									jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
										lstKeys4 = new ArrayList<String>();
										for (int i = 0; i < jsonObjectResponse.names().length(); i++) {
											lstKeys4.add(jsonObjectResponse.names().getString(i));
										}
									Collections.sort(lstKeys4);
									listAssert.assertTrue(lstChannelSku.
											containsAll(lstKeys4),"KEYS ARE MISSING");
									listAssert.assertTrue(
											lstChannelSku.size() == lstKeys4.size(),
											"KEYS COUNT IS DIFFERENT");
								}
							}	
						}
					}
				}
			}
			logInstruction.debug("Test case completed = OneSkuChannelGetTest");
			listAssert.assertAll();
	}catch(Exception e){
		logInstruction.fatal("Test case got failed = OneSkuChannelGetTest");
		e.printStackTrace();
		}
	}
	
	
	/**
	 *This test will validate the PUT api :- route+api/v5/skus/<id>/channel_skus/<id>
	 *@throws Exception 
	 * 
	*/
	@Test(enabled = true, priority = 2)
	public void OneSkuChannelPutTest()throws Exception{
		

		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst=new ArrayList<String>();
		String price = null, mrp = null;
		String body = null;
		OutputStream os = null;
		try{
			logInstruction.debug("Test case started = OneSkuChannelPutTest");
			url = new URL(endpoint + "api/v5/skus");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				for (int i = 0; i < jsonArrayResponse.length(); i++) {
					jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
					lst.add(jsonObject2.get("id").toString());
				}
				for(int var1 = 0; var1 < lst.size(); var1++){
					logInstruction.debug("Requesting URL = " + endpoint+"api/v5/skus/"+lst.get(var1)+"/channel_skus");
					
					url = new URL(endpoint + "api/v5/skus/" + lst.get(var1) + "/channel_skus");
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setDoOutput(true);
					httpURLConnection.setRequestMethod("GET");
					
					if(bearerAccessToken != null && bearerAccessToken.length() > 0){
						httpURLConnection.
							setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
					}
					
					if(httpURLConnection.getResponseCode() == 200){
						listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
						listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
						bufferedReader = new BufferedReader(new InputStreamReader(
								httpURLConnection.getInputStream()));
						jsonArrayResponse = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
						
						for (int var4 = 0; var4 < jsonArrayResponse.length(); var4++) {
							jsonObject3=(JSONObject) jsonArrayResponse.get(var4);
							lstKeys = new ArrayList<String>();
							for (int i = 0; i < jsonObject3.names().length(); i++) {
								
								lstKeys.add(jsonObject3.names().getString(i));
							}
							for(int var5 = 0; var5 < lst.size(); var5++){
								logInstruction.debug("Requesting URL = " 
															+ endpoint + "api/v5/skus/" 
															+ lst.get(var5)+"/channel_skus/"
															+ jsonObject3.get("id"));
								url = new URL(endpoint + "api/v5/skus/" 
															+ lst.get(var5) 
															+ "/channel_skus/" 
															+ jsonObject3.get("id"));
								httpURLConnection = (HttpURLConnection) url.openConnection();
								httpURLConnection.setDoOutput(true);
								price = "750";
								mrp = "900";
								body = "{ \"price\" : " + price + ",\"mrp\" : " + mrp + "}";
								httpURLConnection.setRequestMethod("PUT");
								httpURLConnection.setRequestProperty("Content-Type", "application/json");
								if(bearerAccessToken != null && bearerAccessToken.length() > 0){
									httpURLConnection.
										setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
								}
			                     os = httpURLConnection.getOutputStream();
			                     os.write(body.getBytes());
			                     os.flush();
			                     os.close();
								if(httpURLConnection.getResponseCode() == 200){
									listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
									listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
									bufferedReader = new BufferedReader(new InputStreamReader(
											httpURLConnection.getInputStream()));
									jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);	
									lstKeys4 = new ArrayList<String>();
										for (int i = 0; i < jsonObjectResponse.names().length(); i++) {
											lstKeys4.add(jsonObjectResponse.names().getString(i));
										}
									Collections.sort(lstKeys4);
									listAssert.assertTrue(lstChannelIdSku.
											containsAll(lstKeys4),"KEYS ARE MISSING");
									listAssert.assertTrue(
											lstChannelIdSku.size() == lstKeys4.size(),
											"KEYS COUNT IS DIFFERENT");
									listAssert.assertEquals(jsonObjectResponse.get("price"), price, "PRICE IS NOT UPDATED");
									listAssert.assertEquals(jsonObjectResponse.get("mrp"), mrp, "MRP IS NOT UPDATED");
								}
								else if(httpURLConnection.getResponseCode() == 400){
									listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
					                   		"STATUS CODE IS NOT 400") ;
					 				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
					 						"STATUS LINE IS NOT BAD REQUEST");	
					 				bufferedReader = new BufferedReader(new InputStreamReader(
											httpURLConnection.getErrorStream()));
									jsonArray = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
									listAssert.assertEquals(jsonArray.get(0), 
											"The sale start date field is required when price / sale end date is present."
											, "SALE START DATE ERROR IS NOT PRESENT");
									listAssert.assertEquals(jsonArray.get(1), 
											"The sale end date field is required when price / sale start date is present."
											, "SALE END DATE ERROR IS NOT PRESENT");
								}
								
								httpURLConnection = (HttpURLConnection) url.openConnection();
								httpURLConnection.setDoOutput(true);
								price = "-750";
								mrp = "-900";
								body = "{ \"price\" : " + price + ",\"mrp\" : " + mrp + "}";
								httpURLConnection.setRequestMethod("PUT");
								httpURLConnection.setRequestProperty("Content-Type", "application/json");
								if(bearerAccessToken != null && bearerAccessToken.length() > 0){
									httpURLConnection.
										setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
								}
			                     os = httpURLConnection.getOutputStream();
			                     os.write(body.getBytes());
			                     os.flush();
			                     os.close();
			                     if(httpURLConnection.getResponseCode() == 400){
										listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
						                   		"STATUS CODE IS NOT 400") ;
						 				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
						 						"STATUS LINE IS NOT BAD REQUEST");	
						 				bufferedReader = new BufferedReader(new InputStreamReader(
												httpURLConnection.getErrorStream()));
										jsonArray = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
										listAssert.assertEquals(jsonArray.get(0), 
												"The mrp must be at least 1."
												, "MRP IS UPDATED FOR NEGATIVE VALUE");
										listAssert.assertEquals(jsonArray.get(1), 
												"The price must be at least 1.",
												"PRICE IS UPDATED FOR NEGATIVE VALUE");
										listAssert.assertEquals(jsonArray.get(2), 
												"The price should be less than mrp",
												"PRICE IS UPDATED HIGHER THAN MRP");
			                     }
							}	
						}
					}
				}
			}
			logInstruction.debug("Test case completed = OneSkuChannelPutTest");
			listAssert.assertAll();
	}catch(Exception e){
		logInstruction.fatal("Test case got failed = OneSkuChannelPutTest");
		e.printStackTrace();
		}
	}
	
}
