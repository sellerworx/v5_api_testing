package org.sellerworx.api.v5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sellerworx.api.get.persistent.TestData_API_V5Persistent;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sellerworx.api.utils.JsonConvertor;
import com.sellerworx.api.utils.ListAssert;
import com.sellerworx.api.utils.PropertiesFile;

public class SkuTest {

	//Variables
	private String bearerAccessToken = null;
	private String endpoint = PropertiesFile.getresourcePropertiesData("endpoint");
	private List<String> lst = null;
	private List<String> lstKeys = new ArrayList<String>();
	private List<String> lstKeys1 = new ArrayList<String>();
	private List<String> lstKeys2 = new ArrayList<String>();
	private List<String> lstKeys3 = new ArrayList<String>();
	
	@SuppressWarnings("serial")
	private List<String> lstItems = new ArrayList<String>(){
		{
			add("active"); 
			add("avg_orders_count_per_day"); 
			add("channel_skus"); 
			add("created_at");
			add("id");
			add("image");
			add("original_sku"); 
			add("sku");
			add("stock");
			add("tags");
			add("title");
			add("updated_at");
			add("weight");
		}
	};
	
	@SuppressWarnings("serial")
	private List<String> lstChannelSku = new ArrayList<String>(){
		{
			add("active");
			add("avg_orders_count_per_day");
			add("channel_id");
			add("channel_stock");
			add("competitive_price");
			add("created_at");
			add("currency_code");
			add("id");
			add("image");
			add("is_fbm");
			add("last_synced");
			add("last_upsynced");
			add("local_shipping");
			add("mrp");
			add("national_shipping");
			add("original_sku_id");
			add("price");
			add("sale_end_date");
			add("sale_start_date");
			add("sku");
			add("sku_id");
			add("stock");
			add("title");
			add("updated_at");
			add("zonal_shipping");
		}
	};
	
	@SuppressWarnings("serial")
	private List<String> lstTags = new ArrayList<String>(){
		{
			add("color");
			add("created_at");
			add("id");
			add("name");
			add("pivot");
			add("seller_id");
			add("updated_at");
		}
	};

	//Objects
	private JSONObject jsonObjectResponse = null;
	private JSONArray jsonArrayResponse = null;
	private JSONObject jsonObject = null;
	private JSONObject jsonObject2 = null;
	
	private JSONArray jsonArray = null;
	private JSONArray jsonArray1 = null;
	
	private ListAssert listAssert = new ListAssert();
	JsonConvertor jsonConvertor = new JsonConvertor();
	TestData_API_V5Persistent testData = new TestData_API_V5Persistent();
	static Logger logInstruction = LogManager.getLogger(SkuTest.class.getName());
	
	
	/**
	 * This test will validate the GET api :- route+api/v5/skus
	 * @throws Exception
	 */
	@Test(enabled = true,priority = 1)
	public void AllSkuGetTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		
		try{
			logInstruction.debug("Test case started = AllSkuGetTest");
			logInstruction.debug("Requesting URL : " + endpoint + "api/v5/skus");
			url = new URL(endpoint + "api/v5/skus");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
			
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
				
				for(int var1 = 0; var1 < jsonArrayResponse.length(); var1++){
					lstKeys = new ArrayList<String>();
					jsonObject = (JSONObject) jsonArrayResponse.get(var1);
					for (int i = 0; i < jsonObject.names().length(); i++) {
						lstKeys.add(jsonObject.names().getString(i));
					}
					//Json Array Items checking 
					Collections.sort(lstKeys);
					listAssert.assertTrue(lstItems.
							containsAll(lstKeys),"KEYS ARE MISSING");
					listAssert.assertTrue(
							lstItems.size() == lstKeys.size(),
							"KEYS COUNT IS DIFFERENT");
					
					//Json Object channel_skus object checking
					if(lstKeys.get(2).equals("channel_skus")){
						jsonArray = jsonObject.getJSONArray("channel_skus");
						for(int var2 = 0; var2 < jsonArray.length(); var2++){
							lstKeys1 = new ArrayList<String>();
							jsonObject2 = jsonArray.getJSONObject(var2);
							for(int i = 0; i < jsonObject2.names().length(); i++){
								lstKeys1.add(jsonObject2.names().getString(i));
							}
							Collections.sort(lstKeys1);
							listAssert.assertTrue(lstChannelSku.
									containsAll(lstKeys1), "KEYS ARE MISSING");
							listAssert.assertTrue(
									lstChannelSku.size() == lstKeys1.size(), 
									"KEYS COUNT IS DIFFERENT");
						}
					}
					
					//Json Object Tags object Checking
					if(lstKeys.get(9).equals("tags")){
						jsonArray = jsonObject.getJSONArray("tags");
						for(int var3 = 0; var3 < jsonArray.length(); var3++){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = jsonArray.getJSONObject(var3);
							for(int i = 0; i < jsonObject2.names().length(); i++){
								lstKeys2.add(jsonObject2.names().getString(i));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstTags.
									containsAll(lstKeys2), "KEYS ARE MISSING");
							listAssert.assertTrue(
									lstTags.size() == lstKeys2.size(), 
									"KEYS COUNT IS DIFFERENT");
						//Json Object pivot checking
						if(lstKeys2.get(4).equals("pivot")){
								lstKeys3 = new ArrayList<String>();
								for(int i = 0; i < ((JSONObject)jsonObject2.get("pivot")).length(); i++){
									lstKeys3.add(((JSONObject)jsonObject2.get("pivot")).names().getString(i));
								}
								Collections.sort(lstKeys3);
								listAssert.assertEquals(lstKeys3.get(0),"sku_id","SKU_ID KEY IS NOT PRESENT");
								listAssert.assertEquals(lstKeys3.get(1),"tag_id","TAG_ID KEY IS NOT PRESENT");
						}
						}
					}
				}
				
			}
			logInstruction.debug("Test case completed = AllSkuGetTest");
			listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = AllSkuGetTest");
			e.printStackTrace();
		}
		
	}
	
	
	
	
	/**
	 * This test will validate the GET api :- route+api/v5/skus/<id>
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 2)
	public void OneSkuGetTest()throws Exception{

		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		
		try{
			logInstruction.debug("Test case started = OneSkuGetTest");
			logInstruction.debug("Requesting URL : " + endpoint + "api/v5/skus/1");
			url = new URL(endpoint + "api/v5/skus/1");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("GET");
			
			if(bearerAccessToken != null && bearerAccessToken.length() > 0){
				httpURLConnection.
					setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
			}
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
			
				bufferedReader = new BufferedReader(new InputStreamReader(
						httpURLConnection.getInputStream()));
				jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
				lstKeys = new ArrayList<String>();
				for(int var1 = 0; var1 < jsonObjectResponse.length(); var1++){
						lstKeys.add(jsonObjectResponse.names().getString(var1));
					}
					//Json Array Items checking 
					Collections.sort(lstKeys);
					listAssert.assertTrue(lstItems.
							containsAll(lstKeys),"KEYS ARE MISSING");
					listAssert.assertTrue(
							lstItems.size() == lstKeys.size(),
							"KEYS COUNT IS DIFFERENT");
					
					//Json Object channel_skus object checking
					if(lstKeys.get(2).equals("channel_skus")){
						jsonArray = jsonObjectResponse.getJSONArray("channel_skus");
						for(int var2 = 0; var2 < jsonArray.length(); var2++){
							lstKeys1 = new ArrayList<String>();
							jsonObject2 = jsonArray.getJSONObject(var2);
							for(int i = 0; i < jsonObject2.names().length(); i++){
								lstKeys1.add(jsonObject2.names().getString(i));
							}
							Collections.sort(lstKeys1);
							listAssert.assertTrue(lstChannelSku.
									containsAll(lstKeys1), "KEYS ARE MISSING");
							listAssert.assertTrue(
									lstChannelSku.size() == lstKeys1.size(), 
									"KEYS COUNT IS DIFFERENT");
						}
					}
					
					//Json Object Tags object Checking
					if(lstKeys.get(9).equals("tags")){
						jsonArray = jsonObjectResponse.getJSONArray("tags");
						for(int var3 = 0; var3 < jsonArray.length(); var3++){
							lstKeys2 = new ArrayList<String>();
							jsonObject2 = jsonArray.getJSONObject(var3);
							for(int i = 0; i < jsonObject2.names().length(); i++){
								lstKeys2.add(jsonObject2.names().getString(i));
							}
							Collections.sort(lstKeys2);
							listAssert.assertTrue(lstTags.
									containsAll(lstKeys2), "KEYS ARE MISSING");
							listAssert.assertTrue(
									lstTags.size() == lstKeys2.size(), 
									"KEYS COUNT IS DIFFERENT");
						//Json Object pivot checking
						if(lstKeys2.get(4).equals("pivot")){
								lstKeys3 = new ArrayList<String>();
								for(int i = 0; i < ((JSONObject)jsonObject2.get("pivot")).length(); i++){
									lstKeys3.add(((JSONObject)jsonObject2.get("pivot")).names().getString(i));
								}
								Collections.sort(lstKeys3);
								listAssert.assertEquals(lstKeys3.get(0),"sku_id","SKU_ID KEY IS NOT PRESENT");
								listAssert.assertEquals(lstKeys3.get(1),"tag_id","TAG_ID KEY IS NOT PRESENT");
						}
						}
					}
				}
			logInstruction.debug("Test case completed = OneSkuGetTest");
			listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = OneSkuGetTest");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This test will validate the PUT api :- route+api/v5/skus/<id>
	 * This test will validate stock update for a SKU
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 3)
	public void OneSkuPutTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst = new ArrayList<String>();
		String variable = null;
		String body = null;
		OutputStream os = null;
		try{
				logInstruction.debug("Test case started = OneSkuPutTest");
				url = new URL(endpoint + "api/v5/skus");
				httpURLConnection = (HttpURLConnection) url.openConnection();
				httpURLConnection.setDoOutput(true);
				httpURLConnection.setRequestMethod("GET");
				
				if(bearerAccessToken != null && bearerAccessToken.length() > 0){
					httpURLConnection.
						setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
				}
				if(httpURLConnection.getResponseCode() == 200){
					listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
					bufferedReader = new BufferedReader(new InputStreamReader(
							httpURLConnection.getInputStream()));
					jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
					jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
					for (int i = 0; i < jsonArrayResponse.length(); i++) {
						jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
						lst.add(jsonObject2.get("id").toString());
					}
					for(int lstsize = 0; lstsize < lst.size() ; lstsize++){
					 logInstruction.debug("Requesting URL : " + endpoint + "api/v5/skus/" + lst.get(lstsize));
					 url = new URL(endpoint + "api/v5/skus/" + lst.get(lstsize));
                     httpURLConnection = (HttpURLConnection) url.openConnection();
                     httpURLConnection.setDoOutput(true);
                     variable = "20" ;
                     body = "{\"stock\" : \" " + variable + "\"}";
                     httpURLConnection.setRequestMethod("PUT");
                     httpURLConnection.setRequestProperty("Content-Type", "application/json");
                     
                     if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                             httpURLConnection.
                                     setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                     }
                     
                     os = httpURLConnection.getOutputStream();
                     os.write(body.getBytes());
                     os.flush();
                     os.close();
                     
                     if(httpURLConnection.getResponseCode() == 200){
     					listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
     					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
     					bufferedReader = new BufferedReader(new InputStreamReader(
     							httpURLConnection.getInputStream()));
     					jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
     					listAssert.assertEquals(jsonObjectResponse.get("stock"), variable ,"STOCK IS NOT UPDATED");
     					lstKeys = new ArrayList<String>();
     					for(int var1 = 0; var1 < jsonObjectResponse.length(); var1++){
     							lstKeys.add(jsonObjectResponse.names().getString(var1));
     						}
     						//Json Array Items checking 
     						Collections.sort(lstKeys);
     						listAssert.assertTrue(lstItems.
     								containsAll(lstKeys),"KEYS ARE MISSING");
     						listAssert.assertTrue(
     								lstItems.size() == lstKeys.size(),
     								"KEYS COUNT IS DIFFERENT");
     						
     						//Json Object channel_skus object checking
     						if(lstKeys.get(2).equals("channel_skus")){
     							jsonArray = jsonObjectResponse.getJSONArray("channel_skus");
     							for(int var2 = 0; var2 < jsonArray.length(); var2++){
     								lstKeys1 = new ArrayList<String>();
     								jsonObject2 = jsonArray.getJSONObject(var2);
     								listAssert.assertEquals(jsonObject2.getString("stock"), variable, "STOCK IS NOT UPDATED");;
     								for(int i = 0; i < jsonObject2.names().length(); i++){
     									lstKeys1.add(jsonObject2.names().getString(i));
     								}
     								Collections.sort(lstKeys1);
     								listAssert.assertTrue(lstChannelSku.
     										containsAll(lstKeys1), "KEYS ARE MISSING");
     								listAssert.assertTrue(
     										lstChannelSku.size() == lstKeys1.size(), 
     										"KEYS COUNT IS DIFFERENT");
     							}
     						}
     						
     						//Json Object Tags object Checking
     						if(lstKeys.get(9).equals("tags")){
     							jsonArray = jsonObjectResponse.getJSONArray("tags");
     							for(int var3 = 0; var3 < jsonArray.length(); var3++){
     								lstKeys2 = new ArrayList<String>();
     								jsonObject2 = jsonArray.getJSONObject(var3);
     								for(int i = 0; i < jsonObject2.names().length(); i++){
     									lstKeys2.add(jsonObject2.names().getString(i));
     								}
     								Collections.sort(lstKeys2);
     								listAssert.assertTrue(lstTags.
     										containsAll(lstKeys2), "KEYS ARE MISSING");
     								listAssert.assertTrue(
     										lstTags.size() == lstKeys2.size(), 
     										"KEYS COUNT IS DIFFERENT");
     							//Json Object pivot checking
     							if(lstKeys2.get(4).equals("pivot")){
     									lstKeys3 = new ArrayList<String>();
     									for(int i = 0; i < ((JSONObject)jsonObject2.get("pivot")).length(); i++){
     										lstKeys3.add(((JSONObject)jsonObject2.get("pivot")).names().getString(i));
     									}
     									Collections.sort(lstKeys3);
     									listAssert.assertEquals(lstKeys3.get(0),"sku_id","SKU_ID KEY IS NOT PRESENT");
     									listAssert.assertEquals(lstKeys3.get(1),"tag_id","TAG_ID KEY IS NOT PRESENT");
     							}
     							}
     						}
                     }
                     
                     url = new URL(endpoint + "api/v5/skus/" + lst.get(lstsize));
                     httpURLConnection = (HttpURLConnection) url.openConnection();
                     httpURLConnection.setDoOutput(true);
                     variable = "-20" ;
                     body = "{\"stock\" : \"" + variable + "\"}";
                     httpURLConnection.setRequestMethod("PUT");
                     httpURLConnection.setRequestProperty("Content-Type", "application/json");
                     
                     if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                             httpURLConnection.
                                     setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                     }
                     
                     os = httpURLConnection.getOutputStream();
                     os.write(body.getBytes());
                     os.flush();
                     os.close();
                    
                    listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
                    		"STATUS CODE IS NOT 400") ;
  					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
  							"STATUS LINE IS NOT BAD REQUEST");
  					bufferedReader = new BufferedReader(new InputStreamReader(
 							httpURLConnection.getErrorStream()));
  					jsonArrayResponse = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
 					listAssert.assertTrue(jsonArrayResponse.getString(0).equals("The stock must be at least 0."),
 							"STOCK IS  UPDATED FOR VALUE :" +variable);
 					
 					url = new URL(endpoint + "api/v5/skus/" + lst.get(lstsize));
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setDoOutput(true);
                    variable = "SWX" ;
                    body = "{\"stock\" : \"" + variable + "\"}";
                    httpURLConnection.setRequestMethod("PUT");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    
                    if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                            httpURLConnection.
                                    setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                    }
                    
                    os = httpURLConnection.getOutputStream();
                    os.write(body.getBytes());
                    os.flush();
                    os.close();
                   
                   listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
                   		"STATUS CODE IS NOT 400") ;
 					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
 							"STATUS LINE IS NOT BAD REQUEST");
 					bufferedReader = new BufferedReader(new InputStreamReader(
							httpURLConnection.getErrorStream()));
 					
					jsonArrayResponse = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
					listAssert.assertTrue(jsonArrayResponse.getString(0).equals("The stock must be an integer."),
							"STOCK IS  UPDATED FOR VALUE :" +variable);
				}
		}
				logInstruction.debug("Test case completed = OneSkuPutTest");
				listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = OneSkuPutTest");
			e.printStackTrace();
			}
	}
	
	
	/**
	 * This test will validate the PUT api :- route+api/v5/skus/stock
	 * This test will validate stock update for a SKU
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 4)
	public void MultipleSkuPutTest()throws Exception{
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		bearerAccessToken = PropertiesFile.getresourcePropertiesData("access_token");
		lst = new ArrayList<String>();
		String variable = null;
		String body = null;
		OutputStream os = null;
		try{
				logInstruction.debug("Test case started = MultipleSkuPutTest");
				logInstruction.debug("Requesting URL : " +endpoint + "api/v5/skus/stock" );
				url = new URL(endpoint + "api/v5/skus");
				httpURLConnection = (HttpURLConnection) url.openConnection();
				httpURLConnection.setDoOutput(true);
				httpURLConnection.setRequestMethod("GET");
				if(bearerAccessToken != null && bearerAccessToken.length() > 0){
					httpURLConnection.
						setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
				}
				if(httpURLConnection.getResponseCode() == 200){
					listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
					bufferedReader = new BufferedReader(new InputStreamReader(
							httpURLConnection.getInputStream()));
					jsonObjectResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
					jsonArrayResponse = (JSONArray) jsonObjectResponse.get("items");
					for (int i = 0; i < jsonArrayResponse.length(); i++) {
						jsonObject2 = (JSONObject) jsonArrayResponse.get(i);
						lst.add(jsonObject2.get("id").toString());
					}
					Collections.sort(lst);
					 url = new URL(endpoint + "api/v5/skus/stock" );
                     httpURLConnection = (HttpURLConnection) url.openConnection();
                     httpURLConnection.setDoOutput(true);
                     variable = "20" ;
                     body = "{\"stock\" : " + variable + ", \"skus\" :[" + lst.get(0) +  "]}";
                     httpURLConnection.setRequestMethod("PUT");
                     httpURLConnection.setRequestProperty("Content-Type", "application/json");
                     if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                             httpURLConnection.
                                     setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                     }
                     os = httpURLConnection.getOutputStream();
                     os.write(body.getBytes());
                     os.flush();
                     os.close();
                     if(httpURLConnection.getResponseCode() == 200){
     					listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
     					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
     					bufferedReader = new BufferedReader(new InputStreamReader(
     							httpURLConnection.getInputStream()));
     					jsonObject = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
     					
     					listAssert.assertEquals(jsonObject.names().getString(0), "status",
     							"STATUS KEY IS NOT PRESENT");
     					listAssert.assertEquals(jsonObject.getString("status"),
     							"SUCCESS", "STOCK IS NOT UPDATED");	
                     }
                     url = new URL(endpoint + "api/v5/skus/stock" );
                     httpURLConnection = (HttpURLConnection) url.openConnection();
                     httpURLConnection.setDoOutput(true);
                     variable = "-20" ;
                     body = "{\"stock\" : " + variable + ", \"skus\" :[" + lst.get(0) +  "]}";
                     httpURLConnection.setRequestMethod("PUT");
                     httpURLConnection.setRequestProperty("Content-Type", "application/json");
                     
                     if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                             httpURLConnection.
                                     setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                     }
                     os = httpURLConnection.getOutputStream();
                     os.write(body.getBytes());
                     os.flush();
                     os.close();
                    
                    listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
                    		"STATUS CODE IS NOT 400") ;
  					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
  							"STATUS LINE IS NOT BAD REQUEST");
  					bufferedReader = new BufferedReader(new InputStreamReader(
 							httpURLConnection.getErrorStream()));
  					
 					jsonArray = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
 					listAssert.assertTrue(jsonArray.getString(0).equals("The stock must be at least 0."),
 							"STOCK IS  UPDATED FOR VALUE :" +variable);
 					
 					url = new URL(endpoint + "api/v5/skus/stock");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setDoOutput(true);
                    variable = "SWX" ;
                    body = "{\"stock\" : \"" + variable + "\", \"skus\" :[" + lst.get(0) +  "]}";
                    httpURLConnection.setRequestMethod("PUT");
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    
                    if(bearerAccessToken != null && bearerAccessToken.length() > 0){
                            httpURLConnection.
                                    setRequestProperty("Authorization", "Bearer " + bearerAccessToken);
                    }
                    
                    os = httpURLConnection.getOutputStream();
                    os.write(body.getBytes());
                    os.flush();
                    os.close();
                   
                   listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, 
                   		"STATUS CODE IS NOT 400") ;
 					listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", 
 							"STATUS LINE IS NOT BAD REQUEST");
 					bufferedReader = new BufferedReader(new InputStreamReader(
							httpURLConnection.getErrorStream()));
 					
					jsonArray1 = jsonConvertor.GetResponseAsJSONArray(bufferedReader);
					listAssert.assertTrue(jsonArray1.getString(0).equals("The stock must be an integer."),
							"STOCK IS  UPDATED FOR VALUE :" +variable);
				}
				logInstruction.debug("Test case completed = MultipleSkuPutTest");
				listAssert.assertAll();
		}catch(Exception e){
			logInstruction.fatal("Test case got failed = MultipleSkuPutTest");
			e.printStackTrace();
			}
	}
	
}