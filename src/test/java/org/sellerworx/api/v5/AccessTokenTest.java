package org.sellerworx.api.v5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.sellerworx.api.get.persistent.TestData_API_V5Persistent;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.sellerworx.api.utils.JsonConvertor;
import com.sellerworx.api.utils.ListAssert;
import com.sellerworx.api.utils.PropertiesFile;


public class AccessTokenTest {
	
	//Variables
	private String endpoint = PropertiesFile.getresourcePropertiesData("endpoint");
	private String code = PropertiesFile.getresourcePropertiesData("code") ; 
	private String clientId = PropertiesFile.getresourcePropertiesData("clientId");
	private String clientSecret = PropertiesFile.getresourcePropertiesData("clientSecret");
	private String rediectUrl = PropertiesFile.getresourcePropertiesData("rediectUrl");
	private String RefreshToken = null;
	
	private List<String> lstkey = new ArrayList<String>();
	
	@SuppressWarnings("serial")
	private List<String> lstAccessTokenKeys = new ArrayList<String>() {
	{
		add("access_token");
		add("token_type");
		add("expires_in");
		add("refresh_token");
		}
	};
	
	//objects
	private ListAssert listAssert = new ListAssert();
	JsonConvertor jsonConvertor = new JsonConvertor();
	TestData_API_V5Persistent testData = new TestData_API_V5Persistent();
	static Logger logInstruction = LogManager.getLogger(AccessTokenTest.class.getName());
	
	@Test(enabled = true, priority = 1)
	public void GetAccessTokenValidTest() throws Exception{
		
		Map<String, String> paramsmap = new HashMap<String, String>();
		paramsmap.put("client_id", clientId);
		paramsmap.put("client_secret", clientSecret);
		paramsmap.put("redirect_uri", rediectUrl);
		paramsmap.put("code", code);
		paramsmap.put("grant_type", "authorization_code");
		
		String urlParamsEncodedStr = genURLEncodedString(paramsmap);
		
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		
		try{
			Reporter.log("GetAccessTokenValidTest() is started ", true);
			logInstruction.debug("Test case started = GetAccessTokenValidTest");
			logInstruction.debug("Requesting URL : " + endpoint + "oauth/access_token");
			url = new URL(endpoint + "oauth/access_token");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
			
			OutputStream os = httpURLConnection.getOutputStream();
			os.write(urlParamsEncodedStr.getBytes());
			os.flush();
			os.close();
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				
			bufferedReader = new BufferedReader(new InputStreamReader(
					httpURLConnection.getInputStream()));
			JSONObject jsonResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
			for(int i = 0; i < jsonResponse.length(); i++){
				lstkey.add(jsonResponse.names().getString(i));
			}
			Collections.sort(lstkey);
			listAssert.assertTrue(lstAccessTokenKeys.
					containsAll(lstkey),"KEYS ARE MISSING");
			listAssert.assertTrue(
					lstAccessTokenKeys.size() == lstkey.size(),
					"KEYS COUNT IS DIFFERENT");
			
			testData.setAccessToken(jsonResponse.getString("access_token"));
			testData.setRefreshToken(jsonResponse.getString("refresh_token"));
			}else if(httpURLConnection.getResponseCode() == 400){
				logInstruction.debug("Invalid Access Token");
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, "STATUS CODE IS NOT 400");
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", "STATUS LINE IS NOT BAD REQUEST");	
			}else { 

				listAssert.assertEquals(httpURLConnection.getResponseCode(), 401, "STATUS CODE IS NOT 401");
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Unauthorized", "STATUS LINE IS NOT UNAUTHORIZED");
			}
			
			Reporter.log("GetAcccessTokenValidTest is Completed ", true);
			logInstruction.debug("Test case Completed = GetAcccessTokenValidTest");
			listAssert.assertAll();
		}catch(Exception e){
			logInstruction.debug("Test case Failed = GetAcccessTokenValidTest");
			Reporter.log("GetAcccessTokenValidTest is Failed " + e, true);
			e.printStackTrace();
		}
	}
	
	@Test(enabled = true, priority = 2)
	public void GetRefreshTokenValidTest()throws Exception{
		
		RefreshToken = testData.getRefreshToken();
		
		Map<String, String> paramsmap = new HashMap<String, String>();
		paramsmap.put("client_id", clientId);
		paramsmap.put("client_secret", clientSecret);
		paramsmap.put("redirect_uri", rediectUrl);
		paramsmap.put("refresh_token", RefreshToken);
		paramsmap.put("grant_type", "refresh_token");
		
		String urlParamsEncodedStr = genURLEncodedString(paramsmap);
		BufferedReader bufferedReader = null;
		URL url;
		HttpURLConnection httpURLConnection = null;
		
		try{
			logInstruction.debug("Test case started = GetRefreshTokenValidTest");
			logInstruction.debug("Requesting URL : " + endpoint + "oauth/access_token");
			url = new URL(endpoint + "oauth/access_token");
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
			
			OutputStream os = httpURLConnection.getOutputStream();
			os.write(urlParamsEncodedStr.getBytes());
			os.flush();
			os.close();
			
			if(httpURLConnection.getResponseCode() == 200){
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 200, "STATUS CODE IS NOT 200") ;
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "OK", "STATUS LINE IS NOT OK");
				
			bufferedReader = new BufferedReader(new InputStreamReader(
					httpURLConnection.getInputStream()));
			JSONObject jsonResponse = jsonConvertor.GetResponseAsJSONObject(bufferedReader);
			for(int i = 0; i < jsonResponse.length(); i++){
				lstkey.add(jsonResponse.names().getString(i));
			}
			Collections.sort(lstkey);
			
			listAssert.assertTrue(lstAccessTokenKeys.
					containsAll(lstkey),"KEYS ARE MISSING");
			listAssert.assertTrue(
					lstAccessTokenKeys.size() == lstkey.size(),
					"KEYS COUNT IS DIFFERENT");
			
			testData.setAccessToken(jsonResponse.getString("access_token"));
			PropertiesFile.setresourcePropertiesData(testData.getAccessToken());
			testData.setRefreshToken(jsonResponse.getString("refresh_token"));
			}else if(httpURLConnection.getResponseCode() == 400){
				logInstruction.debug("Invalid Access Token");
				listAssert.assertEquals(httpURLConnection.getResponseCode(), 400, "STATUS CODE IS NOT 400");
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Bad Request", "STATUS LINE IS NOT BAD REQUEST");	
			}else { 

				listAssert.assertEquals(httpURLConnection.getResponseCode(), 401, "STATUS CODE IS NOT 401");
				listAssert.assertEquals(httpURLConnection.getResponseMessage(), "Unauthorized", "STATUS LINE IS NOT UNAUTHORIZED");
			}
			logInstruction.debug("Test case Completed = GetRefreshTokenValidTest");
		}catch(Exception e){
			logInstruction.debug("Test case Failed = GetRefreshTokenValidTest");
			e.printStackTrace();
		}
	}
	
	public String genURLEncodedString(Map<String, String> params){
		
		StringBuilder encodedString = new StringBuilder();
		
		try{
			Iterator<Map.Entry<String, String>> entries = params.entrySet().iterator();
			while(entries.hasNext()){
				Map.Entry<String, String> entry = entries.next();
				encodedString.append(entry.getKey());
				encodedString.append("=");
				encodedString.append(URLEncoder.encode(entry.getValue(),"UTF-8"));
				encodedString.append("&");	
			}
		}catch(UnsupportedEncodingException e){
			Reporter.log("Encoding error : " + e, true);
			}
		return encodedString.toString();
		}
	
	
}
