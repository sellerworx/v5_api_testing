package page.all.common;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.sellerworx.api.utils.PropertiesFile;

import page.object.common.BasePageObject;


/**
 * 
 *
 */
public class LoginPage extends BasePageObject{

	public LoginPage(WebDriver webdriver) {
		super(webdriver);
		// TODO Auto-generated constructor stub
	}
  
	
	public LoginPage enterEmail(String strEmail) throws Exception{
		try{
			List<WebElement> inpEmail = createElement(UIType.ID, "email");
			Wait(1000);
			Reporter.log("Entering email : " + strEmail, true);
			if (inpEmail.get(0).isDisplayed()) {
				inpEmail.get(0).sendKeys(strEmail);

			}
			return new LoginPage(webdriver);
			
		}catch(Exception e){
			Reporter.log(
					"Failed method : enterEmail \nReason : " + e.getMessage(),
					true);
			throw new Exception(e);
		}
	}
	
	public LoginPage enterPassword(String strPassword)throws Exception{
		try{
			List<WebElement> inpPassword = createElement(UIType.ID, "password");
			Wait(1000);
			Reporter.log("Entering Password : " + strPassword, true);
			if(inpPassword.get(0).isDisplayed()){
				inpPassword.get(0).sendKeys(strPassword);
			}
			return new LoginPage(webdriver);
			
		}catch(Exception e){
			Reporter.log("Failed method : enterPassword \n Reason : " + e.getMessage(),
					true);
			throw new Exception(e);
		}
	}
	
	
	public void clickSignInBtn(Boolean TrueFalse)throws Exception{
		try{
			List<WebElement> clkSignBtn = createElement(UIType.ID, "btnLogin");
			if(TrueFalse){
			Wait(1000);
			Reporter.log("Clicking SignIn Button ", true);
			//if(clkSignBtn.get(0).isDisplayed()){
				clkSignBtn.get(0).click();
				Wait(5000);
				//return new Object();
			}else {
				Reporter.log("Not clicking login button", true);
				//return new Login(webdriver);
			//}
			}
		}
		catch(Exception e){
			Reporter.log("Failed method : clickSignInBtn \n Reason : " + e.getMessage(),
					true);
			throw new Exception(e);
		}
	}
	
	public String redirectOauthURL(Boolean TrueFalse)throws Exception{
		try{
			webdriver.get(PropertiesFile.getresourcePropertiesData("oauth"));
			Wait(1000);
			List<WebElement> btnYes = createElement(UIType.XPATH, "//button[@value=1]");
			System.out.println(btnYes.get(0).isDisplayed());
			Wait(5000);
			Reporter.log("Clicking Yes Button  " , true);
			if(TrueFalse){
				btnYes.get(0).click();
				Wait(1000);
				//System.out.println(webdriver.getCurrentUrl().toString());
				String code = webdriver.getCurrentUrl().toString();
				code = code.substring(29);
				return code;
			}else{
				Reporter.log("Not Clicking Yes Button ", true);
				return null;
			}
			
		}catch(Exception e){
			Reporter.log("Failed Method : Redirect Oauth URL \n Reason : " + e.getMessage(),
					true);
			throw new Exception(e);
		}
	}
}
