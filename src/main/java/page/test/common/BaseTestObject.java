package page.test.common;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;

import com.sellerworx.api.utils.PropertiesFile;


/**
 * This class file take care browser sessions
 * 
 */
public class BaseTestObject{
	
	protected static WebDriver webdriver = null;
		
	/**
	 * This block start working when the class is called
	 */
	static{
		startBrowser();
	}
    
	/**
	 * This method is used to start the browser and load the URL.
	 */
	public static void startBrowser(){
		System.setProperty("webdriver.chrome.driver", 
				"src/test/resources/Drivers/chromedriver");
		
		Map<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> chromeOptionsMap = new HashMap<String, Object>();
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");
		
		DesiredCapabilities cap = new DesiredCapabilities().chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		
		webdriver = new ChromeDriver(cap);
		webdriver.get(PropertiesFile.getresourcePropertiesData("url"));
		resizeTest();
	}
	
	public static void resizeTest(){
		webdriver.manage().window().maximize();
	}
	
	@AfterTest
	public static void CloseBrowser(){
		webdriver.quit();
		//startBrowser();
	}
}
