package page.object.common;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * This class will initialized all web elements of the page
 * @author vengatraman
 *
 */

public class BasePageObject {

	//variable
	public WebDriver webdriver;

	//Constructor
	public BasePageObject(WebDriver webdriver){
		this.webdriver = webdriver;
	}

	//Enum
	protected enum UIType{
		ID,NAME,CLASS,TAGNAME,CSS,LINK,XPATH,PARTIALLINK;
	}

	/**
	 * This method is used to finding an element based upon locating mechanism
	 * 
	 * @param uiType
	 * @param strValue
	 * @return WebElemt
	 */
	public List<WebElement> createElement(UIType uiType,String strValue){
		if(uiType.name().equals("ID"))
			return webdriver.findElements(By.id(strValue));
		else if(uiType.name().equals("NAME"))
			return webdriver.findElements(By.name(strValue));
		else if(uiType.name().equals("CLASS"))
			return webdriver.findElements(By.className(strValue));
		else if(uiType.name().equals("TAGNAME"))
			return webdriver.findElements(By.tagName(strValue));
		else if(uiType.name().equals("CSS"))
			return webdriver.findElements(By.cssSelector(strValue));
		else if(uiType.name().equals("LINK"))
			return webdriver.findElements(By.linkText(strValue));
		else if(uiType.name().equals("XPATH"))
			return webdriver.findElements(By.xpath(strValue));
		else if(uiType.name().equals("PARTIALLINK"))
			return webdriver.findElements(By.partialLinkText(strValue));
		else
			return null;
	}

	/**
	 * This method will be used for wait for an element on page
	 *
	 *  @throws InterruptedException
	 */
	public void Wait()throws InterruptedException{
		synchronized (webdriver) {
			webdriver.wait();
		}
	}

	/**
	 * This method will be used for wait for an element on page for given time
	 * 
	 * @param timeout
	 * @throws InterruptedException
	 */
	public void Wait(long timeout)throws InterruptedException{
		synchronized (webdriver) {
			webdriver.wait(timeout);
		}
	}

	protected boolean switchWindowUsingTitle(WebDriver webDriver,
			String title){
		String currentwindow = webDriver.getWindowHandle();
		Set<String> availablewindows = webDriver.getWindowHandles();
		if(!availablewindows.isEmpty()){
			for(String windowId : availablewindows){
				if(webDriver.switchTo().window(windowId).getTitle().equals(title)){
					return true;
				}else {
					webDriver.switchTo().window(currentwindow);
				}

			}

		}
		return false;
	}
}
