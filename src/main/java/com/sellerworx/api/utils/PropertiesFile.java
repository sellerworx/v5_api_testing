package com.sellerworx.api.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesFile {

	public static Properties prop = null;
	public static Properties login = null;
	public static OutputStream os = null;
	
	
	static{
		try{
			File file = new File("src/test/resources/file/resource.properties");
			FileInputStream fileInput = new FileInputStream(file);
			prop = new Properties();
			prop.load(fileInput);
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	static{
		try{
			File file = new File("src/test/resources/file/login.properties");
			FileInputStream fileInput = new FileInputStream(file);
			login = new Properties();
			login.load(fileInput);
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static String getresourcePropertiesData(String strkey){
		return prop.getProperty(strkey);
	}
	
	public static void setresourcePropertiesData(String strkeyvalue){
		try{
			os = new FileOutputStream("src/test/resources/file/resource.properties");
			prop.setProperty("access_token", strkeyvalue);
			prop.store(os, null);
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static String getloginPropertiesData(String strkey){
		return login.getProperty(strkey);
	}
}
