package com.sellerworx.api.utils;

import java.util.Map;

import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Maps;

public class ListAssert extends Assertion{
	private Map<AssertionError, IAssert> error_list = Maps.newHashMap();
	
	/**
	 * This method will add all assertion error in Map
	 * 
	 */
	@Override
	public void executeAssert(IAssert a){
		try{
			a.doAssert();
		}catch(AssertionError ex){
			error_list.put(ex, a);
		}
	}
	
	/**
	 * This method will iterate the map and print the assertion which got failed
	 * 
	 */
	public void assertAll(){
		if(!error_list.isEmpty()){
			StringBuilder sb = new StringBuilder(
					"The following assertion failed: \n");
			boolean first = true;
			for(Map.Entry<AssertionError, IAssert> ae : error_list.entrySet()){
				if(first){
					first = false;
				} else{
					sb.append(", ");
				}
				sb.append(ae.getValue().getMessage());	
			}
			throw new AssertionError(sb.toString());
		}
	}

}
