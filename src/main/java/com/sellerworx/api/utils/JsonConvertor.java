package com.sellerworx.api.utils;

import java.io.BufferedReader;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonConvertor {

public JSONObject GetResponseAsJSONObject(BufferedReader bufferedReader){
		
		StringBuilder sb = new StringBuilder();
		String output;
		if(bufferedReader != null){
			try{
				while((output = bufferedReader.readLine()) != null){
					sb.append(output);
				}
				bufferedReader.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return new JSONObject(sb.toString());
		}else
			return null;
		
	}

public JSONArray GetResponseAsJSONArray(BufferedReader bufferedReader){
	
	StringBuilder sb = new StringBuilder();
	String output;
	if(bufferedReader != null){
		try{
			while((output = bufferedReader.readLine()) != null){
				sb.append(output);
			}
			bufferedReader.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return new JSONArray(sb.toString());
	}else
		return null;
	
}

}
